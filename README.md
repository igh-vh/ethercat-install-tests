[![pipeline status](https://gitlab.com/igh-vh/ethercat-install-tests/badges/main/pipeline.svg)](https://gitlab.com/igh-vh/ethercat-install-tests/-/commits/main)

EtherCAT install Tests
======================

Tests whether the IgH EtherCAT packages at [OBS](https://build.opensuse.org/project/show/science:EtherLab) install cleanly.
DKMS kernel modules are checked and a userspace application is built using CMake.

Currenty tested:
  - Debian 10
  - Debian 11
  - Debian 12
  - Ubuntu 20.04 (Focal)
  - Ubuntu 22.04 (Jammy)
